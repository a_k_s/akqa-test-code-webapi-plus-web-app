﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApp1.Models;

namespace WebApp1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        //When page loads initially
        public ActionResult Convert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Convert(MyDetails details)
        {
            string query = "?name=" + details.Name + "&amount=" + details.Amount;
            string url = "http://localhost:59535/api/Values" + query;
            WebClient client = new WebClient();
           
            string postData = "anything";
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            // Upload the input string using the HTTP POST method.
            byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(postData);
            byte[] byteResult = client.UploadData(url, "POST", byteArray);
            // Decode and display the result.
            var result = Encoding.ASCII.GetString(byteResult);
            ConversionResponse response = new ConversionResponse
            {
                Amount = result,
                Name = details.Name
            };
            return View(response);
        }
        

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

   

}